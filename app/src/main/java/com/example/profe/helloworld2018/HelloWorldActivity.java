package com.example.profe.helloworld2018;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class HelloWorldActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_world);

        Button btnPulsar = (Button) findViewById(R.id.btnpulsar);
        btnPulsar.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        /* TODO feu que la segona vegada
          que pulseu el botó aquest es torni invisible
                    */




        if (view.getId()==R.id.btnpulsar)
        {
            Toast.makeText(this, "Has pulsado el botón", Toast.LENGTH_SHORT).show();
            TextView txtHello = (TextView) findViewById (R.id.txtHello);
            txtHello.setText("No sabes leer!");

        }

    }
}
